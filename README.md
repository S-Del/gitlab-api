# GitLabリリースAPIを利用したリリースページ作成ツール
以下のURLから利用できます  
[GitLabリリース作成ツール](https://www.ne.jp/asahi/ctrl/zxcv/gitlab-api/releases.html)
  
現在は複数ファイルの一括のアップロードに対応していません  
同じタグ名を指定して違うファイルを指定し、リリースページ作成を繰り返すと1つずつ追加することはできます。(裏技？)
## GitLabの日本語化
このREADMEは、日本語に設定したGitLabで手順の説明を行っています。  
以下の方法でGitLabを日本語化できます。
1. ページ右上のアカウントアイコンをクリックし、開いたメニュー内から`Settings`を開く。  
    ![アカウントアイコンの表示位置](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/icon.png)  
    ![個人設定の表示位置](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/icon-settings.png)
2. ページ左のメニュー内から`Preferences`をクリック  
    ![外見表示設定変更の表示位置](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/settings-preferences.png)
3. `Localization` の`Language`を`日本語`に変更し、`Save Changes`ボタンをクリック。  
    ![](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/preferences-localization.png)

## リリースページ作成に必要な準備
### プロジェクトIDの確認
1. [プロジェクト一覧](https://gitlab.com/dashboard/projects)から、リリースページを作成するプロジェクトをクリック。
2. プロジェクト名の下に記載されている`Project ID`の数字を控えておく  
    ![プロジェクトIDの表示位置](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/project_id.png)
### アクセストークンの発行 (発行済みの場合は不要)  
1. ページ右上のアカウントアイコンをクリックし、`設定`を開く。  
    ![アカウントアイコンの表示位置](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/icon.png)  
    ![個人設定の表示位置](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/icon-settings_jp.png)
2. ページ左のメニュー内から`アクセス トークン`をクリック  
    ![アクセストークンの表示位置](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/settings-token.png)
3. トークン作成画面が表示されるので、必要な項目を入力・選択し、`Create personal access token`ボタンをクリック。  
    ※ apiには必ずチェックを入れる  
    ![トークン作成画面](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/tokens-create.png)
4. `Your New Personal Access Token`が表示されるのでトークン文字列を控える  
    ※ トークン文字列はこの作成時にしか表示されないので必ず内容を控えること  
    ※ トークン文字列を紛失・失念した場合は、そのトークンを`Revoke`ボタンで削除して新たに発行すること。  
    ![トークン作成成功画面](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/tokens-allow.png)
### タグの作成 (既に存在するタグを利用する場合は不要)
1. [プロジェクト一覧](https://gitlab.com/dashboard/projects)から、タグを作成するプロジェクトをクリック。
2. ページ左のメニュー内から`リポジトリ`をクリックし、開いたメニュー内から`タグ`をクリック。  
    ![リポジトリメニューのタグ表示位置](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/repository-tag.png)
3. ページ右上の`新しいタグ`ボタンをクリック  
    ![タグ作成ボタン表示位置](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/tag-create_button.png)
4. タグ作成画面が表示されるので、必要な項目を入力し、`タグ作成`ボタンをクリック  
    ※ ここで`Tag name`に入力した名前を控えておく  
    ※ ファイルサイズが10MiB未満のファイルであれば、このページの`リリースノート`からアップロードできる。  
    ![タグ作成画面](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/tag-create_page.png)

## リリースページ作成手順
1. [リリースページ作成ツール](https://www.ne.jp/asahi/ctrl/zxcv/gitlab-api/releases.html)を開く
2. 全ての入力欄に記入し、アップロードが必要な場合は`アップロードファイル`からファイルを選択する。
3. `リリースを作成`ボタンをクリック
4. GitLabにてリリースを作成したプロジェクトを開き、ページ左のメニューからリリースページを開いてリリースが作成されているか確認する。  
    ※ 大きいファイルをアップロードした場合は、そのファイルがリリースページに反映されるまでに時間が掛かる場合がある。  
    ![プロジェクトメニューのリリース表示位置](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/project-releases.png)

## リリースページ削除手順
1. [リリースページ作成ツール](https://www.ne.jp/asahi/ctrl/zxcv/gitlab-api/releases.html)を開く
2. `アクセストークン`・`プロジェクトID`・`タグ名`を記入する
3. `リリースを削除`ボタンをクリック
4. GitLabにてリリースを作成したプロジェクトを開き、ページ左のメニューからリリースページを開いてリリースが削除されているか確認する。  
    ![プロジェクトメニューのリリース表示位置](https://gitlab.com/S-Del/gitlab-imgs/raw/master/GitLab-API/project-releases.png)