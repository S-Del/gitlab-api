"use strict";

window.addEventListener("load", () => {
  document.getElementById("release_create_btn").addEventListener("click", () => {
    let privateToken = document.getElementById("private_token").value;
    if (!privateToken) {
      alert("プライベートトークンの入力は必須です");
      return;
    }

    let projectId = document.getElementById("project_id").value;
    if (!projectId) {
      alert("プロジェクトIDの入力は必須です");
      return;
    }

    let releaseTag = document.getElementById("release_tag").value;
    if (!releaseTag) {
      alert("リリースタグの入力は必須です");
      return;
    }

    let releaseName = document.getElementById("release_name").value;
    if (!releaseName) {
      alert("リリースを作成する場合はリリース名を入力してください");
      return;
    }

    let releaseDesc = document.getElementById("release_desc").value;
    if (!releaseDesc) {
      alert("リリースを作成する場合はリリースの説明を入力してください");
      return;
    }

    const BASE_URI = "https://gitlab.com/api/v4/projects/" + projectId;

    let createReleaseUri = BASE_URI + "/releases";
    let headers = new Headers();
    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json");
    headers.append("PRIVATE-TOKEN", privateToken);
    let body = {
      name: releaseName,
      tag_name: releaseTag,
      description: releaseDesc
    };

    fetch(createReleaseUri, {
      method: "POST",
      mode: "cors",
      headers: headers,
      body: JSON.stringify(body)
    }).then((resp) => {
      if (!resp.ok) {
        alert("リリースページの作成に失敗");
      } else {
        alert("リリースページの作成に成功");
      }
    });
  });
});