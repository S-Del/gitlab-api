"use strict";

window.addEventListener("load", () => {
  document.getElementById("release_delete_btn").addEventListener("click", () => {
    let privateToken = document.getElementById("private_token").value;
    if (!privateToken) {
      alert("プライベートトークンの入力は必須です");
      return;
    }

    let projectId = document.getElementById("project_id").value;
    if (!projectId) {
      alert("プロジェクトIDの入力は必須です");
      return;
    }

    let releaseTag = document.getElementById("release_tag").value;
    if (!releaseTag) {
      alert("リリースタグの入力は必須です");
      return;
    }

    let deleteUri = "https://gitlab.com/api/v4/projects/" + projectId + "/releases/" + releaseTag;
    let headers = new Headers();
    headers.append("PRIVATE-TOKEN", privateToken);

    fetch(deleteUri, {
      method: "DELETE",
      mode: "cors",
      headers: headers
    }).then((resp) => {
      if (resp.ok) {
        alert("リリースページの削除に成功しました");
      } else {
        alert("リリースページの削除に失敗しました");
      }
    });
  });
});
