"use strict";

window.addEventListener("load", () => {
  document.getElementById("upload_file_btn").addEventListener("click", () => {
    let privateToken = document.getElementById("private_token").value;
    if (!privateToken) {
      alert("プライベートトークンの入力は必須です");
      return;
    }

    let projectId = document.getElementById("project_id").value;
    if (!projectId) {
      alert("プロジェクトIDの入力は必須です");
      return;
    }

    let releaseTag = document.getElementById("release_tag").value;
    if (!releaseTag) {
      alert("リリースタグの入力は必須です");
      return;
    }

    let releaseFile = document.querySelector("input[type='file']").files[0];
    if (!releaseFile) {
      alert("アップロードするファイルを選択してください");
      return;
    }

    const BASE_URI = "https://gitlab.com/api/v4/projects/" + projectId;
    let projectUri = undefined;
    let link = {};

    const fileUpload = async () => {
      let uploadUri = BASE_URI + "/uploads";
      let headers = new Headers();
      headers.append("PRIVATE-TOKEN", privateToken);
      let formData = new FormData();
      formData.append("file", releaseFile);

      const resp = await fetch(uploadUri, {
        method: "POST",
        mode: "cors",
        headers: headers,
        body: formData
      });

      if (!resp.ok) {
        alert("リリースファイルのアップロードに失敗");
      }

      const respJson = await resp.json();
      link.name = await respJson.alt;
      link.url = await respJson.url;

      return;
    };

    const getProject = async () => {
      let headers = new Headers();
      headers.append("PRIVATE-TOKEN", privateToken);
      const resp = await fetch(BASE_URI, {
        method: "GET",
        mode: "cors",
        headers: headers
      });

      if (!resp.ok) {
        alert("プロジェクトの取得に失敗");
      }

      const respJson = await resp.json();
      projectUri = await respJson.web_url;

      return;
    };

    const createLink = async () => {
      let createLinkUri = BASE_URI + "/releases/" + releaseTag + "/assets/links";
      let headers = new Headers();
      headers.append("PRIVATE-TOKEN", privateToken);
      headers.append("Content-Type", "application/json");
      let body = {
        name: link.name,
        url: projectUri + link.url
      };

      const resp = await fetch(createLinkUri, {
        method: "POST",
        mode: "cors",
        headers: headers,
        body: JSON.stringify(body)
      });

      if (!resp.ok) {
        alert("リンクの作成に失敗");
      } else {
        alert("リンクの作成に成功しました。\n" + "GitLabでリリースを確認してください。");
      }

      return;
    };

    fileUpload().then(() => {
      getProject().then(() => {
        createLink();
      });
    });

  });
});